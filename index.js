const num = 2;
const expNum= num**3;

console.log(`The cube of ${num} is ${expNum}.`);

let address = ['258 Washington Ave', 'NW', 'California', '90011'];

console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);

let animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 2 in'
}

console.log(`${animal.name} is a  ${animal.type}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);

let numbers = [1,2,3,4,5];

numbers.forEach((number) => {
	console.log(number);
})

let reduceNumber  = numbers.reduce((acc,current) => acc+current,0)
console.log(reduceNumber);

class Dog {
	constructor(name,age,breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = 'Frankie';
myDog.age = 5;
myDog.breed = 'Miniature Dachshund';

console.log(myDog); 